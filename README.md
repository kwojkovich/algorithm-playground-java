# Setup and Run
This project is configured to use `fluentd` for logging. The output currently logs to stdout of the container.
You can inspect the logs in the Terminal window. You can also modify the fluend.conf file to forward to where you desire.
This project is designed to be driven by jUnit tests via `mvn test`.

 1. **Clone this repository**
```
git clone git@gitlab.com:kwojkovich/algorithm-playground-java.git
```
 2. **Build the fluentd docker container**
```
cd algorithm-playground-java/support/fluentd
docker build -t algo-fluentd-test .
FLUENTID=$(docker run -p 24224:24224 -d algo-fluentd-test) && docker logs -f $FLUENTID
```
 3. ** Run the jUnit tests **
 You should see log messages similar to:
```
2019-06-26 10:25:45.000000000 +0000 fluentd.test.linear_search: {"stop":1561544745581,"success":"False","start":1561544745572,"list_size":10000000,"target":2000000000}
2019-06-26 10:25:45.000000000 +0000 fluentd.test.linear_search: {"stop":1561544745625,"success":"True","start":1561544745616,"list_size":10000000,"target":10000000}
```
----
