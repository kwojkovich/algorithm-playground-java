package com.spicyomelet.learning;

import org.junit.Test;

import java.util.stream.IntStream;

import static org.junit.Assert.*;


public class LinearSearchTest
{
    @Test
    public void shouldReturnFalseIfTargetNotFound() {
        int list[] = IntStream.rangeClosed(1, 10000000).toArray();
        int result = LinearSearch.search(list, 2000000000);
        assertEquals(-1, result);
    }

    @Test
    public void shouldReturnIndexIfTargetFound() {
        int list[] = IntStream.rangeClosed(1, 10000000).toArray();
        int result = LinearSearch.search(list, 10000000);
        assertEquals(9999999, result);
    }
}
