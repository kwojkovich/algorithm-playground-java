package com.spicyomelet.learning;
import java.util.HashMap;
import java.util.Map;
import java.sql.Timestamp;

import org.fluentd.logger.FluentLogger;

/**
 * Linear Search
 * Description: This is a searching algorithm that starts at the beginning of an array
 *              and moves one element to the next comparing against the target.
 *
 *              This is similar to going to book store and searching a section
 *              book-by-book until you find the title you're looking for.
 *
 * Performance: This algorithm operates in linear time, meaning that
 *              as the number of items to search increases, the worst-case
 *              performance increases at the same rate. The worst-case big O notation
 *              is O(n). The best-case for this algorithm is the first element of the array
 *              is the target. The worst-case is if the last element of the input array is the target.
 *
 *  Notes
 *   * This can operate on an unsorted list. It _may_ be useful where it is expensive to
 *     pre-sort the input.
 */
public class LinearSearch
{
    private static FluentLogger LOG = FluentLogger.getLogger("fluentd.test");

    public static Integer search(int[] list, int target) {
        // This is purely setup for the logging framework.
        Map<String, Object> data = new HashMap<String, Object>();
        Timestamp start_timestamp = new Timestamp(System.currentTimeMillis());
        data.put("target", target);
        data.put("list_size", list.length);
        data.put("start", start_timestamp.getTime());

        // Starting at element 0, move through each element of the array by 1
        for (int a = 0; a < list.length; a++) {
            // If the element contains the target
            if (list[a] == target) {

                // Stop the timer and log
                Timestamp stop_timestamp = new Timestamp(System.currentTimeMillis());
                data.put("stop", stop_timestamp);
                data.put("success", "True");
                LOG.log("linear_search", data);

                // Return the index (location) of the target within the input array
                return a;
            }
        }
        // Stop the timer and log
        Timestamp stop_timestamp = new Timestamp(System.currentTimeMillis());
        data.put("stop", stop_timestamp.getTime());
        data.put("success", "False");
        LOG.log("linear_search", data);

        // Return false
        return -1;
    }
}
